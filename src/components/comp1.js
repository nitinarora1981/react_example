import React, {Component} from "react";

class DropList extends Component {

    constructor() {
        super();
    }
    render() {
        const listVals = this.props.listVals;

        return (
            <div>
                <select>
                    { listVals.map((i) => <option>{i}</option>)}
                </select>
            </div>
        );
    }
}

export default DropList;