import React, {Component} from "react";
import ReactDom from 'react-dom';
import DropList from "./components/comp1";

class App extends Component {
    constructor() {
        super();
        this.state =
            [1,2,3,4,5,6,7,8,9]
        ;
    }

    render() {
        return (
            <div>
                <div>Hello World!!
                    <DropList listVals = {this.state} />
                </div>
            </div>
        );
    }
}

ReactDom.render(<App/>, document.getElementById('root'))
